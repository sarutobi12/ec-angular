import { Component, OnInit } from '@angular/core';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { IngredientModalComponent } from './ingredient-modal/ingredient-modal.component';
import { Pagination, PaginatedResult } from 'src/app/_core/_model/pagination';
import { IngredientService } from 'src/app/_core/_service/ingredient.service';
import { AlertifyService } from 'src/app/_core/_service/alertify.service';
import { IIngredient } from 'src/app/_core/_model/Ingredient';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent implements OnInit {
  data: IIngredient[];
  ingredient: IIngredient = {
    id: 0,
    name: '',
    code: '',
    percentage: 0,
    createdDate: '',
    supplierID: 0,
    position: 0
  };
  pagination: Pagination;
  page = 1 ;
  currentPage = 1 ;
  itemsPerPage = 10 ;
  totalItems: any ;
  constructor(
    public modalService: NgbModal,
    private ingredientService: IngredientService,
    private alertify: AlertifyService,
    private route: ActivatedRoute
  ) { }
  show: boolean;
  ngOnInit() {
    this.ingredientService.currentIngredient.subscribe(res => {
      if (res === 300) {
      this.getIngredients();
      }
    });
    this.getIngredients();
  }
  printBarcode() {
    this.show = true;
  }
  backList() {
    this.show = false;
  }
  getIngredients() {
    // this.spinner.show();
     this.ingredientService.getIngredients(this.currentPage, this.itemsPerPage)
       .subscribe((res: PaginatedResult<IIngredient[]>) => {
         this.data = res.result;
         this.pagination = res.pagination;
         this.totalItems = res.pagination.totalItems ;
         this.currentPage = res.pagination.currentPage ;
         this.itemsPerPage = res.pagination.itemsPerPage ;
     //    this.spinner.hide();
       }, error => {
         this.alertify.error(error);
       });
   }
  getAll() {
    this.ingredientService.getAllIngredient().subscribe(res => {
      this.data = res;
      // console.log('Get All: ', res);
    });
  }

  delete(ingredient: IIngredient) {
    this.alertify.confirm('Delete Ingredient', 'Are you sure you want to delete this IngredientID "' + ingredient.id + '" ?', () => {
      this.ingredientService.delete(ingredient.id).subscribe(() => {
        this.getIngredients();
        this.alertify.success('Ingredient has been deleted');
      }, error => {
        this.alertify.error('Failed to delete the Ingredient');
      });
    });
  }
  onPageChange($event) {
    this.currentPage = $event;
    this.getIngredients();
  }
  openIngredientModalComponent() {
    const modalRef = this.modalService.open(IngredientModalComponent, { size: 'md' });
    modalRef.componentInstance.ingredient = this.ingredient;
    modalRef.componentInstance.title = 'Add Ingredient';
    modalRef.result.then((result) => {
      // console.log('OpenIngredientModalComponent', result );
    }, (reason) => {
    });
  }
  openIngredientEditModalComponent(item) {
    const modalRef = this.modalService.open(IngredientModalComponent, { size: 'md' });
    modalRef.componentInstance.ingredient = item;
    modalRef.componentInstance.title = 'Edit Ingredient';
    modalRef.result.then((result) => {
      // console.log('openIngredientEditModalComponent', result );
    }, (reason) => {
    });
  }

}
