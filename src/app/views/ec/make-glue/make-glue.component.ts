import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { AccumulationChartComponent, IAccLoadedEventArgs, AccumulationTheme, AccumulationChart } from '@syncfusion/ej2-angular-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { ChartOptions } from 'chart.js';
import { IGlue } from 'src/app/_core/_model/glue';
import { IMakeGlue } from 'src/app/_core/_model/make-glue';
import { IIngredient } from 'src/app/_core/_model/Ingredient';
import { MakeGlueService } from 'src/app/_core/_service/make-glue.service';
import { AlertifyService } from 'src/app/_core/_service/alertify.service';
import { GlueService } from 'src/app/_core/_service/glue.service';
import { ChartDataService } from 'src/app/_core/_service/chart-data.service';
import { GlueIngredientService } from 'src/app/_core/_service/glue-ingredient.service';
import { EditService, ToolbarService, PageService } from '@syncfusion/ej2-angular-grids';
import { FilteringEventArgs } from '@syncfusion/ej2-dropdowns';
import { EmitType } from '@syncfusion/ej2-base';
import { Query } from '@syncfusion/ej2-data';
import { highlightSearch, DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
@Component({
  selector: 'app-make-glue',
  templateUrl: './make-glue.component.html',
  styleUrls: ['./make-glue.component.scss'],
  providers: [ToolbarService, EditService, PageService]
})
export class MakeGlueComponent implements OnInit {
  public glues: IGlue[];
  public makeData: IGlue;
  makeGlue = {
    id: 0,
    name: '',
    code: '',
    ingredients: []
  };

  guidance =
  {
    modelName: 0,
    modelNo: 0,
    input: 0,
    lineID: 0,
    ingredientID: 0,
    glueID: 0
  };
  guidances: any[] = [] ;
  public ingredients: IIngredient[];
  public fields: object = { text: 'name', value: 'id' };
  public fieldsGlue: object = { text: 'name', value: 'id' };
  public inputIngredient: number;
  public weight: any;
  show: boolean;
  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          // console.log('formatter: ', ctx, value);
          // const label = ctx.chart.data.labels[ctx.dataIndex];
          return value + 'g';
        },
      },
    }
  };
  public dataModalName: { [key: string]: Object }[] = [ ];
  public dataModalNo: { [key: string]: Object }[] = [ ];
  public queryString: string;
  public fields2: Object = {
    text: 'name', value: 'id', itemCreated: (e: any) => {
      highlightSearch(e.item, this.queryString, true, 'StartsWith');
    }
  };
  public textName = 'Select Model Name';
  public textNo = 'Select Model No';
  public textGlue = 'Select glue name or glue code';
  public pieChartLabels: string[];
  public pieChartData: any[] = [];
  public pieChartType = 'pie';
  public pieChartPlugins = [pluginDataLabels];
  public isModalNo: boolean ;
  public A: any ;
  public B: any ;
  public C: any ;
  public D: any ;
  public E: any ;
  public B1: any ;
  public C1: any ;
  public D1: any ;
  public E1: any ;
  public existGlue: any = false ;
  @ViewChild('glueDropdownlist')
  public dropDownListObject: DropDownListComponent;
  public suggestions: any ;
  public editSettings: object = {
    showDeleteConfirmDialog: false,
    allowEditing: false,
    allowAdding: false,
    allowDeleting: false,
    mode: 'Normal' };
  public toolbar: string[] = ['Delete', 'Search'];
  public gluess: object[];
  public modelNameid: any ;
  @ViewChild('ddlelement')
  public dropDownListObject1: DropDownListComponent;
  constructor(
    private makeGlueService: MakeGlueService,
    private glueService: GlueService,
    private alertify: AlertifyService,
    private chartDataService: ChartDataService,
    private glueIngredientService: GlueIngredientService
  ) { }
  ngOnInit() {
    this.getAllGlue();
    this.getAllModelName();
  }
  actionBegin(args) {

  }
  clickHandler(args) {

  }
  rowSelected(args) {

  }
  getGlueByModelName(id) {
    this.glueService.getGlueByModelName(id).subscribe((res: any) => {
      this.gluess = res;
    });
  }
  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
      // document.getElementById('btnClear').onclick = () => {
      //   this.dropDownListObject.value = null;
      //   console.log('ngAfterViewInit: ', document.getElementById('glueInput'));
      //   document.getElementById('glueInput').innerText = '';
      // };
  }

  changeModalName($event) {
    if ($event.value === null) {
      this.modelNameid = this.modelNameid ;
      // console.log('null', this.modelNameid);
    } else {
      this.modelNameid = $event.value ;
      // console.log('Notnull', this.modelNameid);
    }
    this.getGlueByModelName(this.modelNameid);
    this.existGlue = true ;
    this.show = false ;
    // this.guidance.modelName = $event.value ;
    // this.getAllModelNo($event.value);
  }
  changeModalNo($event) {
    this.guidance.modelNo = $event.value ;
  }
  async onChangeInput($event) {
    // console.log('onChangeInput: ', $event);
    const { value: weight } = await this.alertify.$swal.fire({
      title: 'How many amount do you want to mix?',
      input: 'number',
      inputPlaceholder: 'Enter amount'
    });
    this.weight = weight;
    if (this.weight) {
      this.alertify.message(`Entered amount: ${this.weight}`);
      this.getGlueWithIngredientByGlueCode($event.target.value);
    }
  }
  getAllGlue() {
    this.makeGlueService.getAllGlues().subscribe((res) => {
      // console.log('getAllGlue', res);
      this.glues = res;
    });
  }

  fillterGlues(source, text) {
    let reslt = source;
    reslt = source.filter( item => {
      if (item.code.includes(text) || item.name.includes(text)) {
        return item;
      }
    });
    return reslt;
  }
  public onFiltering =  (e: FilteringEventArgs) => {
    const query = new Query();
    // console.log('fillter: ', query);
    this.queryString = e.text;
    e.updateData(this.fillterGlues(this.glues, e.text));
  }

  async mixGlue(data): Promise<void> {
    // console.log(data);
    const { value: weight } = await this.alertify.$swal.fire({
        title: 'How many pairs of shoes do you want to mix this glue on?',
        input: 'number',
        inputPlaceholder: 'Enter amount shoes'
      });
    if (data.consumption === '') {
        this.weight = weight ;
      } else {
        this.weight = Math.round(weight * parseFloat(data.consumption)) ;
      }
    if (this.weight) {
      this.alertify.message(`Entered amount shoes: ${this.weight}`);
      this.getGlueWithIngredientByGlueCode(data.code);
      this.dropDownListObject1.value = null;
      } else {
        // console.log('deo co giay de pha') ;
      }
  }

  async onChangeGlues(args): Promise<void> {
   console.log('onChangeGlues : ', args);
   const element: HTMLElement = document.createElement('p');
   if (args.isInteracted) {
    const { value: weight } = await this.alertify.$swal.fire({
      title: 'How many pairs of shoes do you want to mix this glue on?',
      input: 'number',
      inputPlaceholder: 'Enter amount shoes'
    });
    this.weight = weight;
    if (this.weight) {
      this.alertify.message(`Entered kilograms: ${this.weight}`);
      this.getGlueWithIngredientByGlueCode(args.itemData.code);
    }
      } else {
    }
  }

  getAllModelName() {
    this.makeGlueService.getAllModalName().subscribe((res: any) => {
      this.dataModalName = res ;
    });
  }

  getAllModelNo(id) {
    this.makeGlueService.getAllModalNo(id).subscribe((res: any) => {
      this.dataModalNo = res.modelNos ;
    });
  }

  onChange($event, item) {
    console.log('onChange: ', $event);
    const weightNew = parseFloat($event.target.value);
    const weightOld = (Number(this.weight) * item.percentage) / 100;
    if (weightNew > weightOld) {
      // this.weight = (Number(this.weight) + weightNew) - weightOld;
      // this.weight = weightNew / (item.percentage / 100);
       this.alertify.warning('Exceeded weight!', true);
    }
    const items = {
      modelName: this.guidance.modelName,
      modelNo: this.guidance.modelNo,
      input: $event.target.value,
      lineID: 0,
      ingredientID: item.id,
      glueID: this.makeGlue.id
    };
    this.guidances.push(items);
    // console.log(this.guidances);
  }

  Finish() {
    this.makeGlueService.Guidance(this.guidances).subscribe(() => {
      this.alertify.success('The Glue has been finished successfully');
    });
  }

  async change($event) {
    this.show = true;
    const { value: weight } = await this.alertify.$swal.fire({
      title: 'How many kilogram do you want to mix?',
      input: 'number',
      inputPlaceholder: 'Enter kilogram',
      inputAttributes: {
      }
    });
    this.weight = weight;
    if (this.weight) {
      this.alertify.message(`Entered kilogram: ${this.weight}`);
    }
    this.getMakeGlueByGlueID($event.value);
  }

  suggestion(item, i) {
    if (i === 0) { // A
      return  Math.round(this.weight) + 'g';
    } else if (i === 1) { // B
      this.B = Math.round(parseFloat(this.weight)  * item.percentage / 100) + 'g';
      return this.B ;
    } else if (i === 2) { // C
      this.C = Math.round((parseFloat(this.B) + parseFloat(this.weight) ) * item.percentage / 100) + 'g';
      return this.C;
    }  else if (i === 3) {
      this.D = Math.round((parseFloat(this.B) + parseFloat(this.C) + parseFloat(this.weight) ) * item.percentage / 100) + 'g' ;
      return this.D ;
    } else if (i === 4) {
      this.E = Math.round((parseFloat(this.B) + parseFloat(this.C)
      + parseFloat(this.D) + parseFloat(this.weight) ) * item.percentage / 100) + 'g' ;
      return this.E ;
    }
  }
  getMakeGlueByGlueID(id: number) {
    this.makeGlueService.getMakeGlueByGlueID(id).subscribe((res: any) => {
      this.ingredients = res.ingredients as IIngredient[];
      this.pieChartLabels = this.ingredients.map(item => {
        return item.name;
      });
      this.pieChartData = this.ingredients.map(item => {
        return (Math.round(this.weight * item.percentage) / 100);
      });
    });
  }

  // 09978373
  getGlueWithIngredientByGlueCode(code: string) {
    this.makeGlueService.getGlueWithIngredientByGlueCode(code).subscribe((res: any) => {
      if (res.id === 0) {
        this.alertify.warning('Glue Code is not available!', true);
        this.show = false;
      } else {
        this.show = true;
        this.existGlue = false ;
        this.makeGlue = res;
        this.ingredients = res.ingredients as IIngredient[];
        this.loadDataChart();
      }
    });
  }

  loadDataChart() {
    this.pieChartLabels = this.ingredients.map(item => {
      return item.name;
    });
    this.pieChartData = this.ingredients.map((item, i) => {
      if (i === 0) {
        return Math.round(this.weight);
      } else if (i === 1) {
        this.B1 = Math.round(parseFloat(this.weight)  * item.percentage / 100);
        return this.B1 ;
      } else if (i === 2) {
        this.C1 = Math.round((parseFloat(this.B1) + parseFloat(this.weight) ) * item.percentage / 100) ;
        return this.C1 ;
      } else if (i === 3) {
        this.D1 = Math.round((parseFloat(this.B1) + parseFloat(this.C1) + parseFloat(this.weight) ) * item.percentage / 100) ;
        return this.D1 ;
      } else if (i === 4) {
        this.E1 = Math.round((parseFloat(this.B1) + parseFloat(this.C1)
        + parseFloat(this.D1) + parseFloat(this.weight) ) * item.percentage / 100) ;
        return this.E1 ;
      }
    });
  }

  chartHovered($event) {

  }
  chartClicked($event) { }

  mapGlueIngredient(glueIngredient) {
    this.glueIngredientService.mappGlueIngredient(glueIngredient).subscribe( res => {
      this.alertify.success('Glue and Ingredient have been mapping!');
    });
  }
}
