import { ModalNoService } from './../../../_core/_service/modal-no.service';
import { ModalName } from './../../../_core/_model/modal-name';
import { ModalNameService } from './../../../_core/_service/modal-name.service';
import { ISupplier } from './../../../_core/_model/Supplier';
import { IngredientService } from './../../../_core/_service/ingredient.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

import { AccumulationChartComponent, IAccLoadedEventArgs, AccumulationTheme, AccumulationChart } from '@syncfusion/ej2-angular-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { ChartOptions } from 'chart.js';
import { ActivatedRoute } from '@angular/router';
import { NgbModal , NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { GlueModalComponent } from '../glue/glue-modal/glue-modal.component';
import { PageSettingsModel, GridComponent, IEditCell } from '@syncfusion/ej2-angular-grids';
import { EditService, ToolbarService, PageService } from '@syncfusion/ej2-angular-grids';
import { orderDetails } from './data';
import { IGlue } from 'src/app/_core/_model/glue';
import { Pagination, PaginatedResult } from 'src/app/_core/_model/pagination';
import { IIngredient } from 'src/app/_core/_model/Ingredient';
import { GlueIngredientService } from 'src/app/_core/_service/glue-ingredient.service';
import { GlueService } from 'src/app/_core/_service/glue.service';
import { AlertifyService } from 'src/app/_core/_service/alertify.service';
import { ChartDataService } from 'src/app/_core/_service/chart-data.service';
import { IngredientModalComponent } from '../ingredient/ingredient-modal/ingredient-modal.component';
import { Tooltip, TooltipEventArgs } from '@syncfusion/ej2-popups';
import { DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
@Component({
  selector: 'app-glue-ingredient',
  templateUrl: './glue-ingredient.component.html',
  styleUrls: ['./glue-ingredient.component.css'],
  providers: [ToolbarService, EditService, PageService]
})
export class GlueIngredientComponent implements OnInit {
  modalReference: NgbModalRef ;
  data: IGlue[];
  data2 = orderDetails;
  glue: IGlue = {
    id: 0,
    name: '',
    modelNo: '',
    pathName: '',
    materialName: '',
    gluename: '',
    code: '',
    createdDate: '',
    modalNameID: 0,
    partNameID: 0,
    modelNoID: 0,
    materialNameID: 0,
    consumption: ''
  };
  ingredient: IIngredient = {
    id: 0,
    name: '',
    code: '',
    percentage: 0,
    createdDate: '',
    supplierID: 0,
    position: 0
  };
  Editpercentage = {
    glueID: 0,
    ingredientID: 0,
    percentage: 0
  };
  Editallow = {
    glueID: 0,
    ingredientID: 0,
    allow: 0
  };
  show: boolean;
  public pageSettings: PageSettingsModel;
  pagination: Pagination;
  page = 1;
  public glues: object[];
  public supplier: any[] = [];
  public ingredients1: IIngredient[];
  public ingredients2: IIngredient[];
  paginationG: Pagination;
  paginationI: Pagination;
  glueid: number;
  ingridientID: number ;
  percentage: number;
  gIchecked: boolean;
  percentageDefault: any;
  allowDefault: any;
  percentageChange: any;
  allowChange: any;
  public toolbarOptions = ['Search', 'Delete' ];
  public toolbarOptions2 = ['Search', 'Add', 'Delete' ];
  public selectionOptions = { type: 'Multiple', mode: 'Both' };
  public editSettings: object;
  public toolbar: string[];
  public editparams: object;
  public orderidrules: object;
  public customeridrules: object;
  public freightrules: object;
  @ViewChild('grid')
  public grid: GridComponent;
  @ViewChild('gridglue')
  public gridglue: GridComponent;
  @ViewChild('grid2')
  public grid2: GridComponent;
  
  nameGlue: any ;
  detailGlue: boolean ;
  public detailIngredient: any[] = [] ;
  public percentageCount: [] ;
  public percentageCountEdit: [] ;
  public allowCountEdit: [] ;
  public modelName: [] ;
  public modelNo: [] ;
  public percentageEdit: {} ;
  public allowEdit: {} ;
  public totalPercentage: number = 0;
  public fieldsGlue: object = { text: 'name', value: 'id' };
  public textGlue: string = 'Select Model Name';
  public textGluePartname1: string = 'Select ';
  public textGluePartname2: string = 'Select';
  public textGlueMaterial: string = 'Select ';
  public fieldsGlue2: object = { text: 'name', value: 'id' };
  public textGlue2: string = 'Select Model No';
  existGlue: any = false;
  modelNoText: any ;
  modalname: ModalName = {
    id: 0,
    name: '',
    modelNo: '',
    artNo: '',
  };
  modelNameid: any;
  pathNameEdit: any;
  pathNameEdit2: any;
  materialNameEdit: any;
  showBarCode: boolean;
  partname1: any ;
  partname2: any ;
  materialname: any ;
  public dataPosition: object [] =  [
    { id: '0', name: 'none' },
    { id: '1', name: 'A' },
    { id: '2', name: 'B' },
    { id: '3', name: 'C' },
    { id: '4', name: 'D' },
    { id: '5', name: 'E' },
  ];
  public countryParams: IEditCell;
  public ingrediented: object [];
  public partName: object [];
  public partName2: object [];
  public MaterialName: object [];
  public fieldsGlueEdit: object = { text: 'name', value: 'name' };
  public fieldsChemical: object = { text: 'name', value: 'name' };
  public fieldsposition: object = { text: 'name', value: 'name' };
  public tooltip: Tooltip;
  public ingredientID: any ;
  public subID: any ;
  public rowIndex: any = '';
  public glueIngredient2 = {
      ingredientID: 0,
      percentage: 0,
      glueID: 0,
      position: ''
    };
  public valuess: string = 'Badminton';
  @ViewChild('ddlelement')
  public dropDownListObject: DropDownListComponent;
  @ViewChild('ddlelement2')
  public dropDownListObject2: DropDownListComponent;
  @ViewChild('ddlelement3')
  public dropDownListObject3: DropDownListComponent;
  @ViewChild('ddlelement4')
  public dropDownListObject4: DropDownListComponent;
  public A: any ;
  public B: any ;
  public C: any ;
  public D: any ;
  public E: any ;
  public percentageA: any = '' ;
  public percentageB: any = '' ;
  public percentageC: any = '' ;
  public percentageD: any = '' ;
  public percentageE: any = '' ;
  constructor(
    private glueIngredientService: GlueIngredientService,
    private modalNameService: ModalNameService,
    private modalNoService: ModalNoService,
    private glueService: GlueService,
    private alertify: AlertifyService,
    public modalService: NgbModal,
    private chartDataService: ChartDataService,
    private route: ActivatedRoute,
    private ingredientService: IngredientService,
  ) { }

  ngOnInit() {
    this.resolver();
    this.glueService.currentGlue.subscribe(res => {
      if (res) {
        this.getGlueByModelName(this.modelNameid);
      }
    });
    if (this.glue.id === 0) {
      this.showBarCode = false;
      this.genaratorGlueCode();
    } else {
      this.showBarCode = true;
    }
    this.ingredientService.currentIngredient.subscribe(res => {
      if (res === 200) {
         this.getIngredients();
      }
    });
    this.pageSettings = { pageSize: 12 };
    this.paginationI = {
      currentPage: 1,
      itemsPerPage: 10,
      totalItems: 0,
      totalPages: 0
    };
    this.editSettings = { showDeleteConfirmDialog: false, allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Normal' };
    this.toolbar = ['Delete', 'Search'];
    this.orderidrules = { required: true, number: true };
    this.customeridrules = { required: true };
    this.freightrules = { required: true };
    this.editparams = { paramss: { popupHeight: '300px' } };
    this.getAllModelName();
    this.getAllModelNo();
  }

  created(args) {
  }
  onChangeChemical(args) {
    if (args.itemData === null) {
      this.ingredientID = this.ingredientID ;
      this.glue.gluename = '';
    } else {
      this.ingredientID = args.itemData.id;
      this.glue.gluename = args.value ;
    }
  }

  onChangeposition(args, data) {
    if (args.value === 'none') {
      this.delete(this.glueid, data.id);
    } else {
      this.percentage = 0;
      const glueIngredient = {
        ingredientID: data.id,
        percentage: this.percentage,
        glueID: this.glueid,
        position: args.value
      };

      this.mapGlueIngredient(glueIngredient);
    }
  }

  getAllIngredient() {
    this.ingredientService.getAllIngredient().subscribe((res: any) => {
      this.ingrediented = res ;
    });
  }

  create() {
    this.glueService.create1(this.glue).subscribe( () => {
        this.alertify.success('Created successed!');
        this.getGlueByModelName(this.modelNameid);
        this.dropDownListObject.value = null;
        this.dropDownListObject2.value = null;
        this.glue.gluename = '';
        this.dropDownListObject3.value = null;
        this.glue.modelNoID = 0;
        this.dropDownListObject4.value = null;
        this.glue.partNameID = 0 ;
        this.glue.materialNameID = 0 ;
        this.glue.consumption = '',
        this.showBarCode = false;
        this.genaratorGlueCode();
    },
    (error) => {
      this.alertify.error(error);
      this.genaratorGlueCode();
    });
  }

  update() {
    this.glueService.create(this.glue).subscribe( res => {
      if (res) {
        this.alertify.success('Updated successed!');
      }
    });
  }

  save2() {
    if (this.glue.id === 0) {
      if (this.glue.code === '') {
        this.genaratorGlueCode();
      }
      this.create();
      console.log(this.glue.modalNameID);
    } else {
      this.update();
    }
  }

  save() {
    this.modalNameService.create(this.modalname).subscribe(() => {
      this.alertify.success('Add Modal Name Successfully');
      this.modalReference.close() ;
      this.getAllModelName();
    });
  }

  genaratorGlueCode() {
    this.glue.code = this.makeid(8);
  }

  makeid(length) {
    let result           = '';
    let characters       = '0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
   // return '59129032';
  }

  onChangepartNameEdit(args) {
    this.pathNameEdit = args.value ;
  }

  onChangepartNameEdit1(args) {
    this.pathNameEdit2 = args.value ;
  }

  onChangepartName1(args) {
    if (args.itemData === null) {
      this.glue.modelNoID = 0 ;
    } else {
      this.partname1 = args.itemData.name ;
      this.glue.modelNoID = args.value ;
    }
  }

  onChangepartName2(args) {
    if (args.itemData === null) {
      this.glue.partNameID = 0 ;
    } else {
      this.partname2 = args.itemData.name ;
      this.glue.partNameID = args.value ;
    }
  }

  onChangeMaterialNameEdit(args) {
    this.materialNameEdit = args.value ;
  }

  onChangeMaterialName(args) {
    if (args.itemData === null) {
      this.glue.materialNameID = 0;
    } else {
      this.materialname = args.itemData.name ;
      this.glue.materialNameID = args.value ;
    }
  }

  openaddModalName(addModalName) {
    this.modalReference = this.modalService.open(addModalName);
  }

  onChangeModelName(args) {
    this.modelNameid = args.value ;
    this.glue.modalNameID = args.value ;
    this.getGlueByModelName(args.value);
    this.getModelNoByModelName(args.value);
    this.getAllPartName();
    this.getAllPartName2();
    this.getAllMaterialName();
    this.getAllIngredient();
    this.glueService.changeGlue(this.modelNameid);
    this.existGlue = true ;
    this.detailGlue = false ;
  }

  onChangeModelNo(args) {
  }

  getGlueByModelName(id) {
    this.glueService.getGlueByModelName(id).subscribe((res: any) => {
      if (this.ingredientID === undefined) {
        this.glues = res;
      } else {
        this.glueid = res[0].id ;
        this.glueIngredient2.ingredientID = this.ingredientID ;
        this.glueIngredient2.percentage = 100 ;
        this.glueIngredient2.glueID = this.glueid ;
        this.glueIngredient2.position = 'A' ;
        this.mapGlueIngredient(this.glueIngredient2);
      }

    });
  }

  getAllPartName() {
    this.glueService.getAllPartName().subscribe((res => {
      this.partName = res ;
    }));
  }

  getAllPartName2() {
    this.glueService.getAllPartName2().subscribe((res => {
      this.partName2 = res ;
    }));
  }

  getAllMaterialName() {
    this.glueService.getAllMaterialName().subscribe((res => {
      this.MaterialName = res ;
    }));
  }

  getAllModelNo() {
    this.modalNoService.getAllModalNo().subscribe((res: any) => {
      this.modelNo = res ;
    });
  }

  getAllModelName() {
    this.modalNameService.getAllModalName().subscribe((res: any) => {
      this.modelName = res ;
    });
  }

  getModelNoByModelName(id) {
    this.glueService.getModelNoByModelName(id).subscribe((res: any) => {
      this.modelNoText = res.modelNo;
    });
  }

  sortBySup(id) {
    this.subID = id ;
    this.ingredientService.sortBySup(this.glueid, id).subscribe((res: any) => {
      this.ingredients1 = res.list1;
      this.ingredients2 = res.list2;
    });
  }

  getAllSupllier() {
    this.ingredientService.getAllSupplier().subscribe(res => {
      this.supplier = res ;
    });
  }

  getDetail(id) {
    this.glueIngredientService.getDetail(id).subscribe((res: any) => {
      this.nameGlue = res.name;
      this.detailIngredient = res.glueIngredients ;
      this.A = res.name ;
      if (res.glueIngredients[1] === undefined) {
        this.B = '' ;
      } else {
        this.B = res.glueIngredients[1].percentage + '%' + res.glueIngredients[1].ingredientName ;
      }
      if (res.glueIngredients[2] === undefined) {
        this.C = '' ;
      } else {
        this.C = res.glueIngredients[2].percentage + '%' + res.glueIngredients[2].ingredientName ;
      }
      if (res.glueIngredients[3] === undefined) {
        this.D = '' ;
      } else {
        this.D = res.glueIngredients[3].percentage + '%' + res.glueIngredients[3].ingredientName ;
      }
      if (res.glueIngredients[4] === undefined) {
        this.E = '' ;
      } else {
        this.E = res.glueIngredients[4].percentage + '%' + res.glueIngredients[4].ingredientName ;
      }
      if (res.glueIngredients.length > 0) {
        let list: any = [];
        this.percentageCount = res.glueIngredients.map( (item) => {
          return item.percentage;
        });
        let aaa = this.percentageCount;
        let str = this.percentageCount.join('+');
        this.percentageCountEdit = res.glueIngredients.map( (item) => {
          return item;
        });
        this.totalPercentage = this.percentageCount.reduce((acc, cur) => acc + cur, 0);

      } else {
        this.percentageCount = [] ;
        this.percentageCountEdit = [] ;
        this.totalPercentage = 0 ;
      }
    });
  }

  clickHandler(args) {
    switch (args.item.text) {
      case 'Add':
        this.openGlueModalComponent();
        break;
    }
  }

  clickHandlerIngridient(args) {
    switch (args.item.text) {
      case 'Add':
        this.openIngredientModalComponent();
        break;
    }
  }

  // Update or edit
  actionBegin(args) {
    if (args.requestType === 'beginEdit') {
      this.pathNameEdit = args.rowData.pathName ;
      this.materialNameEdit = args.rowData.materialName ;
      this.pathNameEdit2 = args.rowData.modelNo;
      this.detailGlue = false ;
    }
    if (args.requestType === 'save' ) {
      this.glue.id = args.data.id ;
      this.glue.code = args.data.code ;
      this.glue.modalNameID = args.data.modalNameID ;
      this.glue.partNameID = args.data.partNameID ;
      this.glue.materialNameID = args.data.materialNameID ;
      this.glue.pathName = this.pathNameEdit;
      this.glue.modelNo = this.pathNameEdit2;
      this.glue.materialName = this.materialNameEdit;
      this.glue.name = args.data.name;
      this.glue.consumption = args.data.consumption;
      this.glueService.update(this.glue).subscribe( res => {
        this.alertify.success('Updated successed!');
        this.getGlueByModelName(this.modelNameid);
        this.detailGlue = false ;
      });
    }
    if (args.requestType === 'delete') {
      this.glueService.delete(this.glueid).subscribe(() => {
        this.getGlueByModelName(this.modelNameid);
        this.detailGlue = false ;
        this.alertify.success('Glue has been deleted');
      }, error => {
        this.alertify.error('Failed to delete the Glue');
      });
    }
  }

  checkTotalPercentage(percentageEdit, arrPercentageEdit): Number {
    let temp = arrPercentageEdit.map(item => {
      if (item.ingredientID !== percentageEdit.id) {
        return item.percentage;
      } else return 0;
    });
    let numbers: any = temp;
    let length = numbers.push(percentageEdit.percentage);
    let sum = numbers.reduce((acc, cur) => acc + cur, 0);
    return sum;
  }

// edit Percentage
  actionBeginIngridient(args) {
    if (args.requestType === 'beginEdit') {
      this.Editpercentage.glueID = this.glueid;
      this.Editpercentage.ingredientID = args.rowData.id;
      this.percentageDefault = args.rowData.percentage ;
      this.Editallow.glueID = this.glueid;
      this.Editallow.ingredientID = args.rowData.id;
      this.allowDefault = args.rowData.allow ;
    }
    if (args.requestType === 'save') {
      this.percentageEdit = {
        percentage: Number(args.data.percentage),
        id: Number(args.rowData.id)
      };
      this.allowEdit = {
        allow: Number(args.data.allow),
        id: Number(args.rowData.id)
      };
      this.Editpercentage.percentage = args.data.percentage;
      this.Editallow.allow = args.data.allow;
      const percentageChanged = Number(this.percentageDefault) !== Number(this.percentageChange);
      const AllowChanged = Number(this.allowDefault) !== Number(this.allowChange);
      if (percentageChanged) {
        if (args.rowData.status === false) {
          this.alertify.error('Please map glue with ingredient!', true);
          return;
        } else {
          let sum = this.checkTotalPercentage(this.percentageEdit, this.percentageCountEdit);
          if (sum > 100) {
            this.glueIngredientService.editPercentage(this.Editpercentage).subscribe(() => {
              this.alertify.success('Percentages has been changed success ');
              this.getDetail(this.glueid);
              this.sortBySup(this.subID);
            });
            return;
          } else {
              this.glueIngredientService.editPercentage(this.Editpercentage).subscribe(() => {
              this.alertify.success('Percentages has been changed success ');
              this.getDetail(this.glueid);
              this.sortBySup(this.subID);
            });
          }
        }
      }

      if (AllowChanged) {
        if (args.rowData.status === false || this.Editallow.allow > 2) {
          this.alertify.error('Please map glue with ingredient!', true);
          return;
        } else {
          this.glueIngredientService.editAllow(this.Editallow).subscribe(() => {
            this.alertify.success('Allow has been changed success ');
            this.getDetail(this.glueid);
            this.sortBySup(this.subID);
          });
        }
      }
      this.ingredient.id = args.data.id ;
      this.ingredient.name = args.data.name;
      this.ingredient.code = args.data.code;
      this.ingredient.supplierID = args.data.supplierID;
      this.ingredient.position = args.data.position;
      this.ingredientService.update(this.ingredient).subscribe( res => {
        this.alertify.success('Updated successed!');
      });
    }
    if (args.requestType === 'delete') {
      this.ingredientService.delete(this.ingridientID).subscribe(() => {
        this.getIngredients();
        this.alertify.success('Ingredient has been deleted');
      }, error => {
        this.alertify.error('Failed to delete the Ingredient');
      });
    }
  }

  actionComplete(args) {
    this.glueid = args.data.id ;
    if (args.requestType === 'save') {
      this.glue.id = args.data.id ;
      this.glue.name = args.data.name;
      this.glueService.update(this.glue).subscribe( res => {
        this.alertify.success('Updated successed!');
      });
    }
    if (args.requestType === 'delete')  {
      this.alertify.confirm('Delete Glue', 'Are you sure you want to delete this GlueID "' + this.glueid + '" ?', () => {
        this.glueService.delete(this.glueid).subscribe(() => {
          this.getGlues();
          this.alertify.success('Glue has been deleted');
        }, error => {
          this.alertify.error('Failed to delete the Glue');
        });
      });
    }
  }

  rowSelected(args: any) {
    this.glueid = args.data.id;
    this.getIngredients();
    this.detailGlue = true ;
    this.getDetail(args.data.id);
    this.getAllSupllier();
  }

  rowSelectedIngridient(args) {
    this.ingridientID = args.data.id;
  }

  resolver() {
    this.route.data.subscribe(data => {
      this.glues = data.glues.result;
      this.paginationG = data.glues.pagination;
    });
  }

  mapGlue(data) {
    if (!data.status === true) {
      this.percentage = 0;
      const glueIngredient = {
        ingredientID: data.id,
        percentage: this.percentage,
        glueID: this.glueid
      };

      this.mapGlueIngredient(glueIngredient);
    } else {
      this.delete(this.glueid, data.id);
    }
  }

  async onOff($event, item) {
    if ($event.checked) {
      const { value: percentage } = await this.alertify.$swal.fire({
        value: item.percentage,
        title: 'How many kilograms do you want to enter?',
        input: 'number',
        inputPlaceholder: 'Enter KG',
        inputAttributes: {
        }
      });
      this.percentage = Number(percentage);
      const glueIngredient = {
        ingredientID: item.id,
        percentage: this.percentage,
        glueID: this.glueid
      };
      this.mapGlueIngredient(glueIngredient);
      this.alertify.success('Map GlueIngredient Successfully!');
      this.getIngredients();
      this.getDetail(this.glueid);

    } else {
      this.delete(this.glueid, item.id);
    }
  }

  getIngredients() {
    this.glueIngredientService.getIngredients(this.glueid)
      .subscribe((res: any) => {
        this.ingredients1 = res.result.list1;
        this.ingredients2 = res.result.list2;
      }, error => {
        this.alertify.error(error);
      });
  }

  onClickGlue($event, item) {
    this.glueid = item.id;
    this.getIngredients();
  }

  onPageChange($event) {
    this.paginationG.currentPage = $event;
    this.getGlues();
  }

  onPageChangeI($event) {
    this.paginationI.currentPage = $event;
    this.getIngredients();
  }

  getGlues() {
    this.glueService.getAllGlue()
      .subscribe((res: any) => {
        this.glues = res;
      }, error => {
        this.alertify.error(error);
      });
  }

  mapGlueIngredient(glueIngredient) {
    this.glueIngredientService.mappGlueIngredient(glueIngredient).subscribe(res => {
      if (this.subID === undefined) {
        this.sortBySup(0);
        this.getDetail(this.glueid);
        this.ingredientID = undefined ;
        this.rowIndex = 0 ;
        this.getGlueByModelName(this.modelNameid);
        this.alertify.success('Glue and Ingredient have been mapping!');
      } else {
        this.sortBySup(this.subID);
        this.getDetail(this.glueid);
        this.ingredientID = undefined ;
        // this.rowIndex = 0 ;
        this.getGlueByModelName(this.modelNameid);
        this.alertify.success('Glue and Ingredient have been mapping!');
      }
    });
  }

  delete(glueid, ingredient) {
    this.glueIngredientService.delete(glueid, ingredient).subscribe(res => {
      this.alertify.success('GlueIngredient have been deleted!');
      this.getIngredients();
      this.getDetail(glueid);
    });
  }

  deleteGlue(glue) {
    this.alertify.confirm('Delete Glue', 'Are you sure you want to delete this GlueID "' + glue.id + '" ?', () => {
      this.glueService.delete(glue.id).subscribe(() => {
        this.getGlues();
        this.alertify.success('Glue has been deleted');
      }, error => {
        this.alertify.error('Failed to delete the Glue');
      });
    });
  }

  deleteIngridient(ingredient: IIngredient) {
    this.alertify.confirm('Delete Ingredient', 'Are you sure you want to delete this IngredientID "' + ingredient.id + '" ?', () => {
      this.ingredientService.delete(ingredient.id).subscribe(() => {
        this.getIngredients();
        this.alertify.success('Ingredient has been deleted');
      }, error => {
        this.alertify.error('Failed to delete the Ingredient');
      });
    });
  }

  onPageChangeGlue($event) {
    this.pagination.currentPage = $event;
    this.getGlues();
  }

  openGlueModalComponent() {
    const modalRef = this.modalService.open(GlueModalComponent, { size: 'md' });
    modalRef.componentInstance.glue = this.glue;
    modalRef.componentInstance.title = 'Add Glue';
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

  openIngredientModalComponent() {
    const modalRef = this.modalService.open(IngredientModalComponent, { size: 'md' });
    modalRef.componentInstance.ingredient = this.ingredient;
    modalRef.componentInstance.title = 'Add Ingredient';
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }

}
