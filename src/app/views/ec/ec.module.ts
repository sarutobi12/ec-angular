// Angular
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NgxSpinnerModule } from 'ngx-spinner';
// Components Routing
import { ECRoutingModule } from './ec-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';

import { GlueIngredientComponent } from './glue-ingredient/glue-ingredient.component';
import { GlueComponent } from './glue/glue.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { GlueModalComponent } from './glue/glue-modal/glue-modal.component';
import { IngredientModalComponent } from './ingredient/ingredient-modal/ingredient-modal.component';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
// Import ngx-barcode module
import { BarcodeGeneratorAllModule, DataMatrixGeneratorAllModule } from '@syncfusion/ej2-angular-barcode-generator';
import { ChartAllModule, AccumulationChartAllModule, RangeNavigatorAllModule } from '@syncfusion/ej2-angular-charts';
import { ChartsModule } from 'ng2-charts';
import { MakeGlueComponent } from './make-glue/make-glue.component';
import { SwitchModule } from '@syncfusion/ej2-angular-buttons';
import { GridAllModule } from '@syncfusion/ej2-angular-grids';
import { TreeGridAllModule } from '@syncfusion/ej2-angular-treegrid';
import { ModalNameComponent } from './modal-name/modal-name.component';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { ModalNoComponent } from './modal-no/modal-no.component';
import { PlanComponent } from './plan/plan.component';
import { PrintBarCodeComponent } from './print-bar-code/print-bar-code.component';
import { LineComponent } from './line/line.component';
import { SuppilerComponent } from './suppiler/suppiler.component';
import { PartName1Component } from './part-name1/part-name1.component';
import { PartName2Component } from './part-name2/part-name2.component';
import { MaterialNameComponent } from './material-name/material-name.component';
@NgModule({
  imports: [
    ButtonModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    ECRoutingModule,
    NgSelectModule,
    DropDownListModule,
    NgbModule,
    ChartsModule,
    ChartAllModule,
    AccumulationChartAllModule,
    RangeNavigatorAllModule,
    BarcodeGeneratorAllModule,
    DataMatrixGeneratorAllModule,
    SwitchModule,
    TreeGridAllModule,
    GridAllModule
  ],
  declarations: [
    GlueIngredientComponent,
    GlueComponent,
    IngredientComponent,
    GlueModalComponent,
    IngredientModalComponent,
    MakeGlueComponent,
    ModalNameComponent,
    ModalNoComponent,
    PlanComponent,
    PrintBarCodeComponent,
    LineComponent,
    SuppilerComponent,
    PartName1Component,
    PartName2Component,
    MaterialNameComponent
  ]
})
export class ECModule { }
