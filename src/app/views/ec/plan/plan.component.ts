import { ModalNameService } from './../../../_core/_service/modal-name.service';
import { GlueService } from './../../../_core/_service/glue.service';
import { GlueIngredientService } from './../../../_core/_service/glue-ingredient.service';
import { LineService } from './../../../_core/_service/line.service';
import { PlanService } from './../../../_core/_service/plan.service';
import { Plan } from './../../../_core/_model/plan';
import { Component, OnInit } from '@angular/core';
import { AlertifyService } from 'src/app/_core/_service/alertify.service';
import { PageSettingsModel, GridComponent } from '@syncfusion/ej2-angular-grids';
import { NgbModal , NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { EditService, ToolbarService, PageService } from '@syncfusion/ej2-angular-grids';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css'],
  providers: [
    DatePipe
  ]
})
export class PlanComponent implements OnInit {
  public pageSettings: PageSettingsModel;
  public toolbarOptions = ['Search' ];
  public editSettings: object;
  public toolbar: string[];
  public editparams: object;
  public grid: GridComponent;
  modalReference: NgbModalRef ;
  public data: object [];
  searchSettings: any = { hierarchyMode: 'Parent' } ;
  modalPlan: Plan = {
    id: 0,
    title: '',
    lineName: '',
    glueName: '',
    modelName: '',
    quantity: '',
  };
  public textGlueLineName: string = 'Select ';
  public fieldsGlue: object = { text: 'name', value: 'name' };
  public fieldsGlueEdit: object = { text: 'name', value: 'name' };
  public lineName: object [];
  public modelName: object [];
  public glueName: object [];
  lineNameEdit: any;
  glueNameEdit: any ;
  modelNameEdit: any ;
  constructor(
    private route: ActivatedRoute,
    private alertify: AlertifyService,
    public modalService: NgbModal,
    private planService: PlanService,
    private lineService: LineService,
    private glueIngredientService: GlueIngredientService,
    private modalNameService: ModalNameService,
    private glueService: GlueService,
    public datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.pageSettings = { pageSize: 6 };
    this.editparams = { params: { popupHeight: '300px' } };
    this.editSettings = { showDeleteConfirmDialog: false, allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Normal' };
    this.toolbar = ['Add', 'Delete', 'Search'];
    this.getAll();
    this.getAllModelName();
    this.getGlues();
    this.getAllLine();
  }
  getAllModelName() {
    this.modalNameService.getAllModalName().subscribe((res: any) => {
      this.modelName = res ;
    });
  }
  getGlues() {
    this.glueService.getAllGlue()
      .subscribe((res: any) => {
        this.glueName = res;
      }, error => {
        this.alertify.error(error);
      });
  }
  getAllLine() {
    this.lineService.getAllLine().subscribe((res: any) => {
      this.lineName = res ;
    });
  }
  onChangeLineName(args) {
    this.modalPlan.lineName = args.value ;
  }
  onChangeModelName(args) {
    this.modalPlan.modelName = args.value;
  }
  onChangeGlueName(args) {
    this.modalPlan.glueName = args.value;
  }
  onChangeLineNameEdit(args) {
    this.lineNameEdit = args.value;

  }
  onChangeModelNameEdit(args) {
    this.modelNameEdit = args.value;
  }
  onChangeGlueNameEdit(args) {
    this.glueNameEdit = args.value;
  }
  actionBegin(args) {
    if (args.requestType === 'beginEdit') {
      this.lineNameEdit = args.rowData.lineName ;
      this.modelNameEdit = args.rowData.modelName ;
      this.glueNameEdit = args.rowData.glueName ;
    }
    if (args.requestType === 'save' ) {
      this.modalPlan.id = args.data.id ;
      this.modalPlan.title = args.data.title;
      this.modalPlan.lineName = this.lineNameEdit;
      this.modalPlan.glueName = this.glueNameEdit;
      this.modalPlan.modelName = this.modelNameEdit;
      this.modalPlan.quantity = args.data.quantity;

      this.planService.update(this.modalPlan).subscribe( res => {
        this.alertify.success('Updated successed!');
        this.getAll();
      });
    }
  }
  rowSelected(args) {

  }
  openaddModalPlan(addModalPlan) {
    this.modalReference = this.modalService.open(addModalPlan);
  }
  getAll() {
    this.planService.getAll().subscribe((res: any) => {
      // console.log('getAllPlan' , res) ;
      this.data = res ;
    });
  }
  delete(id) {
    this.alertify.confirm('Delete Plan', 'Are you sure you want to delete this Plan "' + id + '" ?', () => {
      this.planService.delete(id).subscribe(() => {
        this.getAll();
        this.alertify.success('Plan has been deleted');
      }, error => {
        this.alertify.error('Failed to delete the Modal Name');
      });
    });
  }
  save() {
    this.planService.create(this.modalPlan).subscribe(() => {
      this.alertify.success('Add Plan Successfully');
      // this.modalReference.close() ;
      this.getAll();
      this.modalPlan.title = '';
      this.modalPlan.quantity = '';
    });
  }
}
