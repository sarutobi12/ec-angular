import { data } from './../glue-ingredient/data';
import { Component, OnInit } from '@angular/core';
import { ModalName } from './../../../_core/_model/modal-name';
import { ModalNameService } from './../../../_core/_service/modal-name.service';
import { AlertifyService } from 'src/app/_core/_service/alertify.service';
import { PageSettingsModel, GridComponent } from '@syncfusion/ej2-angular-grids';
import { NgbModal , NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { EditService, ToolbarService, PageService } from '@syncfusion/ej2-angular-grids';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-modal-name',
  templateUrl: './modal-name.component.html',
  styleUrls: ['./modal-name.component.css'],
  providers: [ToolbarService, EditService, PageService]
})
export class ModalNameComponent implements OnInit {
  public pageSettings: PageSettingsModel;
  public toolbarOptions = ['Search' ];
  public editSettings: object;
  public toolbar: string[];
  public editparams: object;
  public grid: GridComponent;
  modalReference: NgbModalRef ;
  public data: object [];
  searchSettings: any = { hierarchyMode: 'Parent' } ;
  public name: string ;
  modalname: ModalName = {
    id: 0,
    name: '',
    modelNo: '',
    artNo: ''
  };
  constructor(
    private modalNameService: ModalNameService,
    private route: ActivatedRoute,
    private alertify: AlertifyService,
    public modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.pageSettings = { pageSize: 6 };
    this.editparams = { params: { popupHeight: '300px' } };
    this.editSettings = { showDeleteConfirmDialog: false, allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Normal' };
    this.toolbar = ['Add', 'Delete', 'Search'];
    this.getAllModalName();
  }
  actionBegin(args) {
    // console.log(args) ;
    if (args.requestType === 'save') {
      this.modalname.id = args.data.id ;
      this.modalname.name = args.data.name ;
      this.modalNameService.update(this.modalname).subscribe(() => {
        this.alertify.success('Update Modal Name Successfully');
      });
    }
  }
  rowSelected(args) {

  }
  openaddModalName(addModalName) {
    this.modalReference = this.modalService.open(addModalName);
  }
  getAllModalName() {
    this.modalNameService.getAllModalName().subscribe((res: any) => {
      this.data = res ;
    });
  }
  delete(id) {
    this.alertify.confirm('Delete Modal Name', 'Are you sure you want to delete this ModalName ID "' + id + '" ?', () => {
      this.modalNameService.delete(id).subscribe(() => {
        this.getAllModalName();
        this.alertify.success('Modal Name has been deleted');
      }, error => {
        this.alertify.error('Failed to delete the Modal Name');
      });
    });
  }
  save() {
    this.modalNameService.create(this.modalname).subscribe(() => {
      this.alertify.success('Add Modal Name Successfully');
      this.modalReference.close() ;
      this.getAllModalName();
    });
  }
}

