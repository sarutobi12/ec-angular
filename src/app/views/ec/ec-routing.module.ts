import { MaterialNameComponent } from './material-name/material-name.component';
import { PartName2Component } from './part-name2/part-name2.component';
import { PartName1Component } from './part-name1/part-name1.component';
import { SuppilerComponent } from './suppiler/suppiler.component';
import { PlanComponent } from './plan/plan.component';
import { ModalNoComponent } from './modal-no/modal-no.component';
import { ModalNameComponent } from './modal-name/modal-name.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GlueIngredientComponent } from './glue-ingredient/glue-ingredient.component';
import { GlueComponent } from './glue/glue.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { GlueModalComponent } from './glue/glue-modal/glue-modal.component';
import { IngredientModalComponent } from './ingredient/ingredient-modal/ingredient-modal.component';
import { GlueResolver } from '../../_core/_resolvers/glue.resolver';
import { IngredientResolver } from '../../_core/_resolvers/ingredient.resolver';
import { MakeGlueComponent } from './make-glue/make-glue.component';
import { LineComponent } from './line/line.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'ec'
    },
    children: [
      {
        path: 'maintenace-data',
        resolve: { glues: GlueResolver },
        component: GlueIngredientComponent,
        data: {
          title: 'Maintenace Data'
        }
      },
      {
        path: 'guidance',
        component: MakeGlueComponent,
        data: {
          title: 'GuiDance'
        }
      },
      {
        path: 'supplier',
        component: SuppilerComponent,
        data: {
          title: 'Suppiler'
        }
      },
      {
        path: 'modal-name',
        component: ModalNameComponent,
        data: {
          title: 'Modal Name'
        }
      },
      {
        path: 'partname-1',
        component: PartName1Component,
        data: {
          title: 'Part Name 1'
        }
      },
      {
        path: 'partname-2',
        component: PartName2Component,
        data: {
          title: 'Part Name 2'
        }
      },
      {
        path: 'material-name',
        component: MaterialNameComponent,
        data: {
          title: 'Part Name 2'
        }
      },
      {
        path: 'line',
        component: LineComponent,
        data: {
          title: 'Line'
        }
      },
      {
        path: 'modal-no',
        component: ModalNoComponent,
        data: {
          title: 'Modal No'
        }
      },
      {
        path: 'plan',
        component: PlanComponent,
        data: {
          title: 'Plan'
        }
      },
      {
        path: 'glue',
        component: GlueComponent,
        resolve: { glues: GlueResolver },
        data: {
          title: 'Glues'
        }
      },
      {
        path: 'ingredient',
        // resolve: { ingredients: IngredientResolver },
        component: IngredientComponent,
        data: {
          title: 'Ingredient'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ECRoutingModule { }
