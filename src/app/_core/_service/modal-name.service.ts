import { ModalName } from './../_model/modal-name';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    // 'Authorization': 'Bearer ' + localStorage.getItem('token'),
  }),
};
@Injectable({
  providedIn: 'root'
})
export class ModalNameService {
  baseUrl = environment.apiUrlEC;
  ModalNameSource = new BehaviorSubject<number>(0);
  currentModalName = this.ModalNameSource.asObservable();
  constructor(
    private http: HttpClient
  ) { }

  getAllModalName() {
    return this.http.get<ModalName[]>(this.baseUrl + 'ModelName/GetAll', {});
  }

  create(modal: ModalName) {
    return this.http.post(this.baseUrl + 'ModelName/Create', modal);
  }
  update(modal: ModalName) {
    return this.http.put(this.baseUrl + 'ModelName/Update', modal);
  }
  delete(id: number) {
    return this.http.delete(this.baseUrl + 'ModelName/Delete/' + id);
  }
}
