export interface IGlue  {
    id: number;
    name: string;
    modelNo: string;
    pathName: string;
    materialName: string;
    code: string;
    gluename: string;
    createdDate: string;
    modalNameID: number;
    modelNoID: number;
    partNameID: number;
    materialNameID: number;
    consumption: string;
}