export interface Plan {
    id: number;
    title: string;
    lineName: string;
    glueName: string;
    modelName: string;
    quantity: string;
}
