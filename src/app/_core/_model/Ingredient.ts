export interface IIngredient  {
    id: number;
    name: string;
    code: string;
    percentage: number;
    createdDate: string;
    supplierID: number;
    position: number;
}